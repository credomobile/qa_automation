# QA Automation #

Repo for QA automation interview questions


### Instructions ###

Please read these instructions carefully and follow them the best you can. 

### About the application ###

Restful-booker is an open source API that allows you to get, create, update and delete travel bookings.

Documentation Link - https://restful-booker.herokuapp.com/apidoc/index.html

* Write at least 3 test cases for `GetBooking` endpoint.
* Write at least 3 test cases for `CreateBooking` endpoint.
* Write at least 2 test cases for `UpdateBooking` endpoint.
* Write at least 2 test cases for `DeleteBooking` endpoint.

There are also some intentional bugs in the application so mark your test cases as such if you encounter bugs.

### Framework Architecture

* Implement using Python and Pytest framework.
* Make sure your tests are using Auth tokens where applicable.
* Make sure to architect your code in a reusable way(basically applying OOP principle). We would like to see how you
structure your framework, tests and other common bits of code.
* Your tests should be light weight and framework extendable in a efficient way. 

### How to start ###

* Fork this repository into your own Bitbucket branch.
* Create a new branch, write your tests and commit changes to this repo.
* Submit a pull request back to this repository with your solution when finished.